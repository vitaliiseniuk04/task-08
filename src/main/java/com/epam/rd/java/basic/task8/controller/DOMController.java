package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController implements Comparator<Flower> {
    private static ArrayList<Flower> flowers = new ArrayList<>();
    private String xmlFileName;
    DocumentBuilderFactory factory;

    static String currentElement = "";
    static String name, soil, origin, stemColour, leafColour, measureAve = "", measureTemp = "", measureWater = "", multiplying;
    static int aveLenFlower, tempreture, watering;
    static String lighting;
    static String information;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        factory = DocumentBuilderFactory.newInstance();
    }

    public ArrayList<Flower> getList() throws IOException, SAXException {
        //    parse(new File("D:\\git projects\\task-08\\src\\main\\java\\com\\epam\\rd\\java\\basic\\task8\\"+xmlFileName));
        return flowers;
    }

    public void parse(File xml) throws IOException, SAXException, ParserConfigurationException {


        DocumentBuilder builder = factory.newDocumentBuilder();
        //System.out.println(xml.getAbsoluteFile());
        Document document = builder.parse(xml);

        NodeList employeeElements = document.getDocumentElement().getElementsByTagName("flower");

        for (int i = 0; i < employeeElements.getLength(); i++) {
            Node employee = employeeElements.item(i);
            printInfoAboutAllChildNodes(employee.getChildNodes());


            flowers.add(new Flower(name, soil, origin, stemColour, leafColour, measureAve, measureTemp, measureWater, multiplying, aveLenFlower, tempreture, watering, lighting));
            measureWater = "";
            measureAve = "";
            measureTemp = "";
        }
    }

    private static void printInfoAboutAllChildNodes(NodeList list) {
        for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if(node.getTextContent()!=null){

                information = node.getTextContent().replace("\n", "").trim();
            }else
                information="";

            //  System.out.println(information);
            currentElement = node.getNodeName();

            NamedNodeMap d = node.getAttributes();


            if (currentElement.equals("name"))
                name = information;
            if (currentElement.equals("soil"))
                soil = information;
            if (currentElement.equals("origin"))
                origin = information;
            if (currentElement.equals("stemColour"))
                stemColour = information;
            if (currentElement.equals("leafColour"))
                leafColour = information;
            if (currentElement.equals("aveLenFlower")) {
                aveLenFlower = Integer.valueOf(information);
                measureAve = d.item(0).getNodeValue();
            }
            if (currentElement.equals("tempreture")) {
                tempreture = Integer.valueOf(information);
                measureTemp = d.item(0).getNodeValue();

            }
            if (currentElement.equals("watering")) {
                watering = Integer.valueOf(information);
                measureWater = d.item(0).getNodeValue();
            }

            if (currentElement.equals("lighting")) {
                lighting = d.item(0).getNodeValue();
            }
            if (currentElement.equals("multiplying")) {
                multiplying = information;
            }

            if (node.hasChildNodes())
                printInfoAboutAllChildNodes(node.getChildNodes());
        }
    }


    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getWatering()- o2.getWatering();
    }
}
