package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements Comparator<Flower> {
    SAXParserFactory factory;
    SAXParser parser;
    private String xmlFileName;
    static int atributeLength = 0;
    private static ArrayList<Flower> flowers = new ArrayList<>();


    public SAXController(String xmlFileName) throws ParserConfigurationException, SAXException {
        this.factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        this.parser = factory.newSAXParser();
        this.xmlFileName = xmlFileName;
    }

    public void parse(File xml) throws IOException, SAXException {
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File("input.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xml));
            parser.parse(xml,new XMLHandler());
        } catch (IOException | SAXException e) {
            System.out.println("Exception: "+e.getMessage());

        }


    }

    public ArrayList<Flower> getList() throws IOException, SAXException {
    //    parse(new File("D:\\git projects\\task-08\\src\\main\\java\\com\\epam\\rd\\java\\basic\\task8\\"+xmlFileName));
        return flowers;
    }
    public void sortFlowers(){
        Collections.sort(flowers);
    }

    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getTempreture()- o2.getTempreture();
    }

    private static class XMLHandler extends DefaultHandler {
        String currentElement = "";
        String name, soil, origin, stemColour, leafColour, measureAve="", measureTemp="", measureWater="", multiplying;
        int aveLenFlower, tempreture, watering;
        String lighting;

        @Override
        public void startDocument() throws SAXException {
        }

        @Override
        public void endDocument() throws SAXException {
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

                currentElement = localName;



            if (currentElement.equals("aveLenFlower")) {
               // atributeLength = attributes.getLength();
                //for (int i = 0; i < atributeLength; i++)
                    measureAve += attributes.getValue("measure");
            }

            if (currentElement.equals("tempreture")) {
                atributeLength = attributes.getLength();
                for (int i = 0; i < atributeLength; i++)
                    measureTemp += attributes.getValue(i);

            }
            if (currentElement.equals("lighting")) {
                String temp = "";
                atributeLength = attributes.getLength();
                for (int i = 0; i < atributeLength; i++)
                    temp += attributes.getValue(i);
                lighting = ((temp.equals("yes")) ? "yes" : "no");
            }
            if (currentElement.equals("watering")) {
                atributeLength = attributes.getLength();
                for (int i = 0; i < atributeLength; i++)
                    measureWater += attributes.getValue(i);
            }

        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            currentElement = "";
            if (localName.equals("flower")) {
                flowers.add(new Flower(name, soil, origin, stemColour, leafColour, measureAve, measureTemp, measureWater, multiplying, aveLenFlower, tempreture, watering, lighting));
                measureWater="";
                measureAve="";
                measureTemp="";

            }

        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String information = new String(ch, start, length);


            if (!information.isEmpty()) {
                if (currentElement.equals("name"))
                    name = information;
                if (currentElement.equals("soil"))
                    soil = information;
                if (currentElement.equals("origin"))
                    origin = information;
                if (currentElement.equals("stemColour"))
                    stemColour = information;
                if (currentElement.equals("leafColour"))
                    leafColour = information;
                if (currentElement.equals("aveLenFlower"))
                    aveLenFlower = Integer.valueOf(information);
                if (currentElement.equals("tempreture"))
                    tempreture = Integer.valueOf(information);
                if (currentElement.equals("watering"))
                    watering = Integer.valueOf(information);
                if (currentElement.equals("multiplying"))
                    multiplying = information;
                if (currentElement.equals("leafColour"))
                    leafColour = information;

            }
        }

        @Override
        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        }

    }
// PLACE YOUR CODE HERE

}