package com.epam.rd.java.basic.task8;

import java.util.Comparator;

public class Flower implements Comparable<Flower> {
    String name, soil, origin, stemColour, leafColour, measureAve, measureTemp, measureWater, multiplying;
    int aveLenFlower, tempreture, watering;
    String lighting;
    public Flower(){

    }
    public Flower(String name, String soil, String origin, String stemColour, String leafColour, String measureAve, String measureTemp, String measureWater, String multiplying, int aveLenFlower, int tempreture, int watering, String lighting) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.measureAve = measureAve;
        this.measureTemp = measureTemp;
        this.measureWater = measureWater;
        this.multiplying = multiplying;
        this.aveLenFlower = aveLenFlower;
        this.tempreture = tempreture;
        this.watering = watering;
        this.lighting = lighting;
    }
    @Override
    public int compareTo(Flower e) {
        return  this.getName().compareTo(e.getName());
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", measureAve='" + measureAve + '\'' +
                ", measureTemp='" + measureTemp + '\'' +
                ", measureWater='" + measureWater + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", tempreture=" + tempreture +
                ", watering=" + watering +
                ", lighting='" + lighting + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public String getMeasureAve() {
        return measureAve;
    }

    public void setMeasureAve(String measureAve) {
        this.measureAve = measureAve;
    }

    public String getMeasureTemp() {
        return measureTemp;
    }

    public void setMeasureTemp(String measureTemp) {
        this.measureTemp = measureTemp;
    }

    public String getMeasureWater() {
        return measureWater;
    }

    public void setMeasureWater(String measureWater) {
        this.measureWater = measureWater;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public int getTempreture() {
        return tempreture;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String isLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }
}
