package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;



/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements Comparator<Flower> {

	private String xmlFileName;
	static int atributeLength = 0;
	private static ArrayList<Flower> flowers = new ArrayList<>();
	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Flower> getList() throws IOException, SAXException {
		//    parse(new File("D:\\git projects\\task-08\\src\\main\\java\\com\\epam\\rd\\java\\basic\\task8\\"+xmlFileName));
		return flowers;
	}


	public  void parseXMLfile(String fileName) throws FileNotFoundException, XMLStreamException {

		Flower flower = null;
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));

		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				//System.out.println("~~~");
				StartElement startElement = nextEvent.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case "flower":
						flower= new Flower();
						break;
					case "soil":
						nextEvent = reader.nextEvent();
						flower.setSoil(nextEvent.asCharacters().getData());
					//	System.out.println(nextEvent.asCharacters().getData());
						break;
					case "origin":
						nextEvent = reader.nextEvent();
						flower.setOrigin(nextEvent.asCharacters().getData());
					//	System.out.println(nextEvent.asCharacters().getData());
						break;
					case "stemColour":
						nextEvent = reader.nextEvent();
						flower.setStemColour(nextEvent.asCharacters().getData());
					//	System.out.println(nextEvent.asCharacters().getData());
						break;
					case "name":
						nextEvent = reader.nextEvent();
						flower.setName(nextEvent.asCharacters().getData());
					//	System.out.println(nextEvent.asCharacters().getData());
						break;

					case "leafColour":
					nextEvent = reader.nextEvent();
						flower.setLeafColour(nextEvent.asCharacters().getData());
						//System.out.println(nextEvent.asCharacters().getData());

						break;
					case "aveLenFlower":
						nextEvent = reader.nextEvent();
						flower.setAveLenFlower(Integer.valueOf(nextEvent.asCharacters().getData()));
						flower.setMeasureAve(startElement.getAttributeByName(new QName("measure")).getValue());

						//System.out.println(nextEvent.asCharacters().getData());

						break;
					case "tempreture":
						nextEvent = reader.nextEvent();
						flower.setTempreture(Integer.valueOf(nextEvent.asCharacters().getData()));
						flower.setMeasureTemp(startElement.getAttributeByName(new QName("measure")).getValue());

						//System.out.println(nextEvent.asCharacters().getData());

						break;
					case "lighting":
					nextEvent = reader.nextEvent();
						flower.setLighting(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
						//System.out.println(nextEvent.asCharacters().getData());

						break;
					case "watering":
						nextEvent = reader.nextEvent();
						flower.setWatering(Integer.valueOf(nextEvent.asCharacters().getData()));
						flower.setMeasureWater(startElement.getAttributeByName(new QName("measure")).getValue());

						//System.out.println(nextEvent.asCharacters().getData());

						break;
					case "multiplying":
						nextEvent = reader.nextEvent();
						flower.setMultiplying(nextEvent.asCharacters().getData());
						//System.out.println(nextEvent.asCharacters().getData());

						break;

				}
			}
			if (nextEvent.isEndElement()) {
				EndElement endElement = nextEvent.asEndElement();
				if (endElement.getName().getLocalPart().equals("flower")) {
					flowers.add(flower);
				}
			}

		}

	}

	@Override
	public int compare(Flower o1, Flower o2) {
		return o1.getAveLenFlower()-o2.getAveLenFlower();
	}

	// PLACE YOUR CODE HERE

}