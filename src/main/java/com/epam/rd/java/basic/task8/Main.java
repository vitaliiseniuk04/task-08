package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        domController.parse(new File( "input.xml"));

        // sort (case 1)
        // PLACE YOUR CODE HERE
        ArrayList<Flower> dom = domController.getList();
        dom.stream().forEach(System.out::println);

        // save
        String outputXmlFile = "output.dom.xml";
        intoXML(outputXmlFile,dom);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        saxController.parse(new File(xmlFileName));
        saxController.sortFlowers();

        ArrayList<Flower> sax = saxController.getList();
        // sort  (case 2)
        // PLACE YOUR CODE HERE
        System.out.println("sax");
        sax.stream().forEach(System.out::println);
        System.out.println("sax");
        // save
        outputXmlFile = "output.sax.xml";
        intoXML(outputXmlFile,sax);
        // PLACE YOUR CODE HERE

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        staxController.parseXMLfile(xmlFileName);
        ArrayList<Flower> stax=  staxController.getList();
        stax.stream().forEach(System.out::println);


        // sort  (case 3)
        // PLACE YOUR CODE HERE

        // save
        outputXmlFile = "output.stax.xml";
        intoXML(outputXmlFile,stax);
        // PLACE YOUR CODE HERE
    }
    static  void intoXML(String outputXmlFile, List<Flower> list) throws IOException {
        File xml = new File(outputXmlFile);
        String s="";
        s+="<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n";
        for(int i=0;i<list.size();i++){
            s+="<flower>\n";
            s+=String.format("<%s>%s</%s>\n","name", list.get(i).getName(),"name");
            s+=String.format("<%s>%s</%s>\n","soil", list.get(i).getSoil(),"soil");
            s+=String.format("<%s>%s</%s>\n","origin", list.get(i).getOrigin(),"origin");

            s+="<visualParameters>\n";
            s+=String.format("<%s>%s</%s>\n","stemColour", list.get(i).getStemColour(),"stemColour");
            s+=String.format("<%s>%s</%s>\n","leafColour", list.get(i).getLeafColour(),"leafColour");
            s+=String.format("<%s measure=\"%s\">%s</%s>\n","aveLenFlower",list.get(i).getMeasureAve(), list.get(i).getAveLenFlower(),"aveLenFlower");
            s+="</visualParameters>\n";

            s+="<growingTips>\n";
            s+=String.format("<%s measure=\"%s\">%s</%s>\n","tempreture",list.get(i).getMeasureTemp(), list.get(i).getTempreture(),"tempreture");
            s+=String.format("<%s lightRequiring=\"%s\"/>\n","lighting",list.get(i).getLighting(),"lighting");
            s+=String.format("<%s measure=\"%s\">%s</%s>\n","watering",list.get(i).getMeasureWater(), list.get(i).getWatering(),"watering");
            s+="</growingTips>\n";

            s+=String.format("<%s>%s</%s>\n","multiplying", list.get(i).getMultiplying(),"multiplying");
            s+="</flower>\n";
        }
        s+="</flowers>";
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputXmlFile));
        writer.write(s);
        writer.close();

    }
}
